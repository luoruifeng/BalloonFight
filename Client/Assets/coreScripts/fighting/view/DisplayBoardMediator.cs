﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.coreScripts.fighting.view
{
    public class DisplayBoardMediator : FightingBaseEventMediator
    {
        [Inject]
        public DisplayBoardView view { get; set; }
        public override void OnRegister()
        {
            UpdateListeners(true);
            base.OnRegister();
        }
        protected override void UpdateListeners(bool enable)
        {
            dispatcher.UpdateListener(enable, GameConfig.DisplayBoardEvent.UPDATE, FreshHP);
             base.UpdateListeners(enable);
        }

        protected override void OnGameRestart()
        {
            view.slider.value = 1;
            base.OnGameRestart();
        }
        public void FreshHP(IEvent e)
        {
            var data = e as DisplayBoardEventData;
            view.slider.value -= data.hp;
            if ((view.slider.value -= data.hp) <= 0)
            {
                CustomPropsEventData d = new CustomPropsEventData();
                d.type = GameConfig.PropsState.PROPS_COMMAND;
                d.propStatus = GameConfig.PropsState.LOSE;
                dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND,d);
            }
        }
    }
}
