﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class OperationMediator : FightingBaseEventMediator
    {
        [Inject]
        public OperationView view { get; set; }

        [Inject]
        public DataBaseCommon database { get; set; }
        [Inject]
        public User user { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        protected override void UpdateListeners(bool enable)
        {
            //view.dispatcher.AddListener(GameConfig.CoreEvent.GAME_RESTART, GameReset);
            view.dispatcher.UpdateListener(enable, GameConfig.OperationEvent.NORMAL_TOUCH, OnOperation);
            base.UpdateListeners(enable);
        }

        protected override void InitData()
        {
            var config = database.GetConfigByID(user.HeroId, database.HeroConfigList);
            view.delayHR = config.HR_Delay;
            view.delayVH = config.VH_Delay;
            base.InitData();
        }

        private void OnOperation(IEvent e)
        {
            CustomEventData data = e as CustomEventData;
            e.type = GameConfig.CoreEvent.USER_TOUCH;

            dispatcher.Dispatch(GameConfig.CoreEvent.USER_TOUCH, data);
        }

        //public void GameReset() 
        //{
        //    dispatcher.Dispatch(GameConfig.CoreEvent.GAME_RESTART);
        //}
    }
}
