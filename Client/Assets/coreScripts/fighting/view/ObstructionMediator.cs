﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.fighting.view
{
    public class ObstructionMediator : FightingBaseEventMediator
    {
        [Inject]
        public ObstructionView view { get; set; }
        public override void OnRegister()
        {
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            if (view)
                UpdateListeners(false);
        }
        protected override void UpdateListeners(bool enable)
        {
            view.dispatcher.UpdateListener(enable, GameConfig.ObstructionModel.ON_HIT,OnHit);
            base.UpdateListeners(enable);
        }

        private void OnHit(IEvent e) 
        {
            CustomHitEventData data = e as CustomHitEventData;  
            data.type = GameConfig.CoreEvent.OBSTRUCTION_HIT;
            dispatcher.Dispatch(GameConfig.CoreEvent.OBSTRUCTION_HIT,data);
        }
    }
}
