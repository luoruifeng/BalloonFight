﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.fighting.view
{
    public class testMediator:EventMediator
    {
        [Inject]
        public testView view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        protected void UpdateListeners(bool enable)
        {
            view.dispatcher.UpdateListener(enable, GameConfig.PropsState.STARTGAME, StartGame);
        }

        protected void StartGame(IEvent e)
        {
            dispatcher.Dispatch(GameConfig.CoreEvent.GAME_START);
        }

    }
}
