﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.coreScripts.fighting.model
{
    public class GameOverEventData
    {
        public GameConfig.PlayerState playerState { get; set; }
    }
}
