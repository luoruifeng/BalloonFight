﻿using System;
using UnityEngine;

using strange.extensions.command.impl;
using strange.extensions.command.api;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using Assets.coreScripts.fighting.util;
using Assets.coreScripts.main.model;


namespace Assets.coreScripts.common.SequenceCommand
{
    public  class StartAppCommand:Command
    {
        //[Inject(ContextKeys.CONTEXT_VIEW)]
        //public GameObject contextView { get; set; }

        [Inject(ContextKeys.CONTEXT)]
        public IContext context { get; set; }

        public override void Execute()
        {
            if (context != Context.firstContext)
            {
                Debug.Log("context != Context.firstContext");
                return;
            }

            Debug.Log("StartAppCommand.Execute");
        }

    }
}
